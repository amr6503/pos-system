<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::prefix('report')->group(function () {
    Route::get('daily/{day}','ReportController@Daily');
    Route::get('monthly/{month}','ReportController@Monthly');
    Route::get('yearly/{month}','ReportController@Yearly');
});

Route::Resource('/suplyer','SuplyerController');
Route::Resource('/product','ProductController');
Route::get('/ajax/ajax','ProductController@AjaxData');
Route::Resource('/sales','SaleController');
