<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sale;
use App\Product;
use App\Suplyer;
use Carbon\Carbon;

class ReportController extends Controller
{
   public function Daily($day)
   {
   	  $CurrentYear = now()->year;
   	  $CurrentMonth = now()->month;

   	  $sales= Sale::whereYear('created_at', '=', $CurrentYear)
   	  				->whereMonth('created_at', '=', $CurrentMonth)
   	  				->whereDay('created_at', '=', $day)->get();
   	  return view('reports.day',['sales'=>$sales]);

   }

   public function Monthly($month)
   {
   	  $CurrentYear = now()->year;

   	  $sales= Sale::whereYear('created_at', '=', $CurrentYear)
   	  				->whereMonth('created_at', '=', $month)
   	  				->orderBy('created_at','asc')
   	  				->get()
   	  				->groupBy(function ($val) {return Carbon::parse($val->created_at)->format('d');});
   	  				// dd($sales);

   	  return view('reports.month',['sales'=>$sales]);
   }



   public function Yearly($year)
   {
   	  $sales= Sale::whereYear('created_at', '=', $year);
   	  return view('reports.index',['sales'=>$sales]);
   }
}
