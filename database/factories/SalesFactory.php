<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Sale;
use Faker\Generator as Faker;

$factory->define(Sale::class, function (Faker $faker) {
	$product = App\Product::orderByRaw('RAND()')->first();
    return [
        'product_id' =>$product->id,
        'amount' => $faker->randomDigitNot(0),
        'created_at' => $faker->dateTime($max = 'now'),
        'updated_at' => $faker->dateTime($max = 'now'),
    ];
});
