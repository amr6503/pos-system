@extends('layouts.app')
@section('title')
    الموردين
@endsection
@section('content')
	<table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">  البيان   </th>
      <th scope="col">القيمة   </th>

    </tr>
  </thead>
  <tbody>
  	<tr>
      <td >  الكود  </td>
      <td>{{$product->id}} </td>
    </tr>
    <tr>
       <td >المورد  </td>
      <td>{{$product->suplyer->name}} </td>
    </tr>
    <tr>
      <td >  الصنف  </td>
      <td>{{$product->name}} </td>
    </tr>
        <tr>
      <td >  شراء  </td>
      <td>{{$product->net_price}} </td>
    </tr>
    <tr>
      <td >  بيع   </td>
      <td>{{$product->sell_price}} </td>
    </tr>
    <tr>
      <td >  سابق  </td>
      <td>{{$product->at_storage}} </td>
    </tr>
    <tr>
      <td >  مباع  </td>
      <td>{{$product->sold}} </td>
    </tr>
    <tr>
      <td >  حالي   </td>
      <td>{{$product->at_stprage - $product->sold}} </td>
    </tr>
  </tbody>
</table>

@endsection