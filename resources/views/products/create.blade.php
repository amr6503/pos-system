@extends('layouts.app')
@section('title')
    الموردين
@endsection
@section('content')
	<section  class="container mt-5 text-right">
		<form class="" action="/product" method="post">
		  @CSRF

		  <div class="form-group d-flex">
		    <label for="exampleInputPassword1" class="col-2"> اسم  المنتج </label>
		    <input name="product_name" type="text" class="form-control col-8" id="exampleInputPassword1" placeholder=" اسم  المنتج  " required>
		  </div>

		  <div class="form-group d-flex">
		    <label for="exampleInputEmail1" class="col-2"> المورد  </label>
		    <select class="form-control col-8 InputHieght" id="exampleFormControlSelect1" name="suplyer_id" required>
			    <option selected disabled> دار  النشر </option>
		    	@foreach($suplyers as $suplyer)
			    	<option value="{{$suplyer->id}}">{{$suplyer->name}}</option>
		    	@endforeach
			</select>
		  </div>

		  <div class="form-group d-flex">
		    <label  class="col-2"> سعر الشراء </label>
		    <input  name="net_price" type="number" class="form-control col-8"  placeholder=" سعر الشراء  " required>
		  </div>

		  <div class="form-group d-flex">
		    <label  class="col-2"> سعر البيع  </label>
		    <input name="sell_price" type="number" class="form-control col-8"  placeholder=" سعر البيع  " required>
		  </div>

		  <div class="form-group d-flex">
		    <label  class="col-2"> اضافة لرصيد المخزن الحالي  </label>
		    <input name="at_storage" type="number" class="form-control col-8"  placeholder="" required>
		  </div>
 
			<div class="w-100 text-left">
			  <button type="submit" class="btn btn-primary">اضافة منتج جديد</button>
			</div>
		</form>
	</section>
	

@endsection