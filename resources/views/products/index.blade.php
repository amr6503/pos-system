	@extends('layouts.app')
	@section('title')
	    المخزن 	
	@endsection
	@section('content')
	<section  class="container mt-5 text-right">
		<form class="row" action="/sales" method="post">
		  @CSRF
		  {{-- selling a product field  --}}
		  <div class="form-group d-flex col-5	">
		    <label  class="col-2"> اسم  المنتج </label>
			
		  {{-- search thing  --}}
		  <select class="form-control js-data-example-ajax" id="selectto">
		  	<option selected disabled>اسم الكتاب </option>

		  	@foreach($selection as $key=>$value)
		  	<option value="{{$value->id}}">{{$value->product_name}}</option>
		  	@endforeach
		  </select>
			{{-- end of search  --}}
		    
		  </div>
		  {{-- end of selling a product field  --}}
		  <div class="form-group d-flex col-3">
		    <label  class="col-4"> الكمية  </label>
			<input type="number"  name="amount"  class="form-control col-8"  placeholder=" اادخل الكمية  " list="products" required>
		  </div>


			<div class="w-100 col-3">
			  <button type="submit" class="btn btn-primary"> بيع الكتاب </button>
			</div>
		</form>
	</section>
	<section class="container">
		<table class="table table-striped">
		  <thead class="thead-light">
		    <tr>
		      <th scope="col">الكود </th>
		      <th scope="col"> المنتج  </th>
		      <th scope="col"> المورد  </th>
		      <th scope="col"> بيانات  المنتج   </th>
		    </tr>
		  </thead>
		  <tbody>
		    	@foreach($products as $product)
		    	 <tr>
					<th scope="row">{{$product->id}}</th>
					 <td>{{$product->product_name}}</td>
					 <td>{{$product->suplyer->name}}</td>
					 <td class="d-flex">
					 	<a href="/suplyer/{{$product->id}}" class="btn btn-primary h-50"> عرض </a>
					 	<form method="POST" action="suplyer/{{$product->id}}">
					 		<button type="submit" class="btn btn-danger"> حذف  </button>
					 		@CSRF
					 		@method('delete')
					 	</form>
					 </td>
				 </tr>
				@endforeach
		  </tbody>
		</table>
		{{ $products->links() }}
	</section>




		

	@endsection
	@section('script')
	<script src="{{asset('js/select2.min.js')}}"></script>
		<script type="text/javascript">

		
        


		// var toto = $.get('{{URL::to('ajax/ajax')}}',function(data){
		// 	global thedata= data;
		// });
		// $(console.log(countries));

		// $('#test').autocomplete({
		// 	     serviceUrl:  "http://127.0.0.1:8000/ajax/ajax" ,
		// 	    onSelect: function (suggestion) {
		// 	        alert('You selected: ' + suggestion.value + ', ' + suggestion.data);
		// 	    }
		// 	});
		

		$('.js-data-example-ajax').select2();
		
			
		</script>

	@endsection