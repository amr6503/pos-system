@extends('layouts.app')
@section('title')
    الموردين
@endsection
@section('content')
<section class="container">
  <table class="table table-striped">
    <thead>
      <tr>
        <th scope="col">  الكود  </th>
        <th scope="col">المورد  </th>
        <th scope="col">  الصنف  </th>
        <th scope="col">  شراء  </th>
        <th scope="col">  بيع   </th>
        <th scope="col">  سابق  </th>
        <th scope="col">  مباع  </th>
        <th scope="col">  حالي   </th>
      </tr>
    </thead>
    <tbody>
      {{-- {{dd($suplyer->products)}} --}}
    	@foreach($suplyer->products as $product)
      <tr>
        <?php
         $storage = $product->at_storage;
         $sold = $product->sold;
         $current = $storage - $sold;
          ?>
        <th scope="row">{{$product->id}}</th>
        <td>{{$suplyer->name}}</td>
        <td>{{$product->product_name}}</td>
        <td>{{$product->net_price}}</td>
        <td>{{$product->sell_price}}</td>
        <td>{{$storage}}</td>
        <td>{{$sold}}</td>
        <td>{{$current}}</td>
      </tr>
      @endforeach
    </tbody>
  </table>
</section>

@endsection