@extends('layouts.app')
@section('title')
    الموردين
@endsection
@section('content')
<section  class="container mt-5 mb-3">
		<form class="d-flex row" action="/suplyer" method="post">
		  <div class="form-group d-flex col-8  text-center">
		    <label  class=" col-2"> اسم المورد </label>
		    <input type="text" name="SuplyerName" class="form-control col-10"   placeholder=" اسم المورد ">
		  </div>
		  <button type="submit" class="btn btn-primary ">اضافة مورد جديد</button>
		  @CSRF
		</form>
</section>
<section class="container">
	<table class="table table-striped">
	  <thead class="thead-light">
	    <tr>
	      <th scope="col">الكود </th>
	      <th scope="col"> المورد  </th>
	      <th scope="col"> بيانات المورد  </th>
	    </tr>
	  </thead>
	  <tbody>
	    	@foreach($suplyers as $suplyer)
	    	 <tr>
				<th scope="row">{{$suplyer->id}}</th>
				 <td>{{$suplyer->name}}</td>
				 <td class="d-flex">
				 	<a href="/suplyer/{{$suplyer->id}}" class="btn btn-primary h-50"> عرض </a>
				 	<form method="POST" action="suplyer/{{$suplyer->id}}">
				 		<button type="submit" class="btn btn-danger"> حذف  </button>
				 		@CSRF
				 		@method('delete')
				 	</form>
				 </td>
			 </tr>
			@endforeach
	  </tbody>
	</table>
</section>




	

@endsection