@extends('layouts.app')
@section('title')
    الموردين
@endsection
@section('content')
	<section  class="container mt-5 text-right">
		<form class="row" action="/sales" method="post">
		  @CSRF

		  <div class="form-group d-flex col-6">
		    <label  class="col-2"> اسم  المنتج </label>
			<input id="product_name" name=""  class="form-control col-10"  placeholder=" اسم  المنتج  " list="products" required>
			<datalist id="products">
		    	@foreach($products as $product)
			    	<option>{{$product->product_name}}</option>
		    	@endforeach
		    </datalist>
		    
		  </div>

		  <div class="form-group d-flex col-3">
		    <label  class="col-4"> الكمية  </label>
			<input type="number"  name="amount"  class="form-control col-8"  placeholder=" اادخل الكمية  " list="products" required>
		  </div>


			<div class="w-100 col-3">
			  <button type="submit" class="btn btn-primary">اضافة منتج جديد</button>
			</div>
		</form>
	</section>
	

@endsection