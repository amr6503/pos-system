	@extends('layouts.app')
	@section('title')
	    التقرير الشهري  	
	@endsection
	@section('content')
	<section class="container">
		<table class="table table-striped">
		  <thead class="thead-light">
		    <tr>
		      <th scope="col"> الكود   </th>
		      <th scope="col"> اسم الكتاب   </th>
		      <th scope="col"> دار النشر   </th>
		      <th scope="col"> الكمية المباعة </th>
		      <th scope="col">اجمالي سعر الكتب  </th>
		      <th scope="col">اجمالي تكلفة الكتب  </th>
		      <th scope="col">اجمالي الربح من الكتب </th>
		      <th scope="col"> التاريخ </th>

		    </tr>
		  </thead>
		  <tbody>
		  	<?php 
		  	global $SellPrice; 
		  	global $NetPrice; 
		  	?>
		    	@foreach($sales as $sale)
		    	 <tr>
					<th scope="row">{{$sale->id}}</th>
					 <td>{{$sale->product->product_name}}</td>
					 <td>{{$sale->product->suplyer->name}}</td>
					 <td>{{$sale->amount}}</td>
					 <td>{{$sale->product->sell_price * $sale->amount}}</td>
					 <td>{{$sale->product->net_price * $sale->amount}}</td>
					 <td>{{ ($sale->product->sell_price * $sale->amount) - ($sale->product->net_price * $sale->amount)}}</td>
					 <td>{{$sale->created_at}}</td>
					 <?php $SellPrice= $SellPrice + ($sale->product->sell_price * $sale->amount) ;?>
					 <?php $NetPrice= $NetPrice + ($sale->product->net_price * $sale->amount) ;?>
				 </tr>
				@endforeach
				<tr>
					<th scope="row">الاجمالي </th>
					 <td></td>
					 <td></td>
					 <td>{{$sales->sum('amount')}}</td>
					 <td>{{ $NetPrice }}</td>
					 <td>{{ $SellPrice }}</td>
					 <td>{{ $SellPrice - $NetPrice  }}</td>
					 {{-- <td>{{$sale->created_at}}</td> --}}
				 </tr>
		  </tbody>
		</table>
	</section>




		

	@endsection