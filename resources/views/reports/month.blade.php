	@extends('layouts.app')
	@section('title')
	    التقرير الشهري  	
	@endsection
	@section('content')
	<section class="container">
		<table class="table table-striped">
		  <thead class="thead-light">
		    <tr>
		      <th scope="col"> اليوم   </th>
		      <th scope="col">عدد الكتب الاجمالي  </th>
		      <th scope="col">اجمالي تكلفة الكتب  </th>
		      <th scope="col">اجمالي سعر الكتب  </th>
		      <th scope="col">اجمالي الربح من الكتب </th>

		    </tr>
		  </thead>
		  <tbody>
		  	<?php 
			  	global $SellPrice; 
			  	global $NetPrice; 
			  	global $TotalAmount; 
			  	global $TotalNetPrice; 
			  	global $TotalSellPrice; 
		  	?>
		    	@foreach($sales as$key=> $day)
				<tr>
					<th scope="row">{{$key}}</th>
					 <td>{{$day->sum('amount')}}</td>
					 <td>{{$NetPrice}}</td>
					 <td>{{ $SellPrice }}</td>
					 <td>{{ $SellPrice - $NetPrice }}</td>
					 
					 {{-- these three lines are to get totals of the month  --}}
					 <?php $TotalAmount = $TotalAmount +$day->sum('amount')  ?>
					 <?php $TotalNetPrice =$TotalNetPrice + $NetPrice   ?>
					 <?php $TotalSellPrice =$TotalSellPrice + $SellPrice   ?>
					 {{-- this line to reset the variables to get the correct values --}}
					 <?php $amount=0; $SellPrice=0; $NetPrice=0;  ?>
					 {{-- this loop is to get the right values from the relation first then stor it in variables  --}}
		    	@foreach($day as $sale)			 
					 <?php $SellPrice= $SellPrice + ($sale->product->sell_price * $sale->amount) ;?>
					 <?php $NetPrice= $NetPrice + ($sale->product->net_price * $sale->amount) ;?>
				 </tr>
				@endforeach
				@endforeach
				<tr>
					<th scope="row"> اجمالي الشهر </th>
					 <td>{{$TotalAmount}}</td>
					 <td>{{ $TotalNetPrice }}</td>
					 <td>{{ $TotalSellPrice }}</td>
					 <td>{{ $TotalSellPrice - $TotalNetPrice  }}</td>
				 </tr>
		  </tbody>
		</table>
	</section>




		

	@endsection