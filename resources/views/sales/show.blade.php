@extends('layouts.app')
@section('title')
    الموردين
@endsection
@section('content')
	<table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">  الكود  </th>
      <th scope="col">المورد  </th>
      <th scope="col">  الصنف  </th>
      <th scope="col">  شراء  </th>
      <th scope="col">  بيع   </th>
      <th scope="col">  سابق  </th>
      <th scope="col">  مباع  </th>
      <th scope="col">  حالي   </th>
    </tr>
  </thead>
  <tbody>
  	@foreach($sales  as $product)
    <tr>
      <th scope="row">1</th>
      <td>Mark</td>
      <td>Otto</td>
      <td>@mdo</td>
    </tr>
    @endforeach
  </tbody>
</table>

@endsection