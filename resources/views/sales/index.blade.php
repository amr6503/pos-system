	@extends('layouts.app')
	@section('title')
	    المخزن 	
	@endsection
	@section('content')
	<section class="container">
		<table class="table table-striped">
		  <thead class="thead-light">
		    <tr>
		      <th scope="col"> المنتج  </th>
		      <th scope="col"> المورد  </th>
		      <th scope="col"> عدد القطع   </th>
		      <th scope="col"> سعر القطعة   </th>
		      <th scope="col"> السعر الاجمالي    </th>
		      <th scope="col"> التاريخ   </th>
		    </tr>
		  </thead>
		  <tbody>
		    	@foreach($sales as $sale)
		    	 <tr>
					<th scope="row">{{$sale->product->product_name}}</th>
					 <td>{{$sale->product->suplyer->name}}</td>
					 <td>{{$sale->amount}}</td>
					 <td>{{$sale->product->sell_price}}</td>
					 <td>{{$sale->product->sell_price * $sale->amount}}</td>
					 <td>{{$sale->created_at}}</td>
				 </tr>
				@endforeach
		  </tbody>
		</table>
	</section>




		

	@endsection