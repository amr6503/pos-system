<html>
    <head>
        <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.min.css')}}"/>
        <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}"/>
        {{-- select2 style  --}}
        <link href="{{asset('css/select2.min.css')}}" rel="stylesheet" />
        
        <script src="{{asset('js/jquery-3.4.1.js')}}"></script>
        <script src="{{asset('js/bootstrap.min.js')}}"></script>
        {{-- <script src="{{asset('Autocomplete/scripts/jquery.mockjax.js')}}"></script> --}}
        <script src="{{asset('Autocomplete/src/jquery.autocomplete.js')}}"></script>
        {{-- <script src="{{asset('Autocomplete/scripts/countries.js')}}"></script> --}}
        {{-- <script src="{{asset('Autocomplete/scripts/demo.js')}}"></script> --}}
        <title> @yield('title')</title>
        <meta charset="utf-8">
    </head>
    <body>
        @yield('content')
    </body>
    @yield('script')
</html>